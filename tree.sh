#!/bin/bash
echo ""
echo "****************************"
echo "* DIRECTORY TREE STRUCTURE *"
echo "****************************"
echo Date list created:
 /bin/date
echo Created by:
echo  $USER
echo Contents:
 /bin/tree | grep directories
echo "****************************"
echo ""
/bin/tree --noreport
