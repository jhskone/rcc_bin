#!/bin/bash
# 
# Get priority of current jobs in queue
# This is meant to be incorporated into script that can reassign priority of
# currently pending jobs.
# Required: 
#  <name_of_partition> passed as a string to identify the partition where the job
#                      that is to be modified is running.
# Optional: 
#  <user_name>   passed as a string identifying the user name to list jobs for
#                 If user_name is not specified it defaults to the env variable $USER

if [ $# -lt 1 ]; then 
  cat << EOF

     **********************************
     * Error: Slurm JobID is required *
     **********************************
     Usage: 
           grprior.sh <slurm_jobid> 
     Required: <slurm_jobid>
               This is the jobid of the
               job that should have its
               priority pushed to the top 
               of the queue.

EOF
  exit 1
fi
#
# Need to determine which partition the specified job belongs to
part=`squeue -j $1 -h  -O partition`
#echo $part
#
# Get priorty of highest queued job in partition 
top_prio=`squeue -p $part -h -O Prioritylong | head -1`
#echo "Current highest priority in queue is $top_prio" 
#
# Set value to increment priority for updating job
increment=5000
#
# Update the priority of the requested jobid
new_prio=$((top_prio + increment))
  scontrol update jobid=$1 priority=$new_prio
echo " "
echo "       JobID $1 has been updated with the new priority $new_prio within the $part partition"
echo " "
squeue -p $part -O JOBID,USERNAME,PARTITION,NAME,Numnodes,State,Reason,Prioritylong | head -n4

#### BELOW IS FOR LIST FEED -- NEED TO UPDATE FUNCTIONALITY FOR THIS ####
#Get list of jobids for USER
#squeue -u $USER -h -o  '\''%.18i %.9P %.20j %.8u %.4t %.10M %.6D %.6C %.20R %.10S %Z' | awk '{print $2}'

#counter=1
# while [ $counter -le 24 ]
#  do  
#   echo $counter
#   job=$(($1 + $counter))
#   scontrol update jobid=$job priority=200000
#   echo $job
#   ((counter++))
# done
