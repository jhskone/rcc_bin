#!/usr/bin/env bash
#
# Script to list SU balance as reported by accounting db for each PI account.
#
file_ext=`date '+%Y-%m-%d'`
echo "$file_ext"
get_balance() {
 val1=`accounts balance --acc=$1 | grep "| $1" | awk -F ' ' '{print $2}'`
 val2=`accounts balance --acc=$1 | grep "| $1" | awk -F ' ' '{print $4}'`
 val3=`accounts balance --acc=$1 | grep "| $1" | awk -F ' ' '{print $6}'`
 val4=`accounts balance --acc=$1 | grep "| $1" | awk -F ' ' '{print $8}'`
if [[ $val4 == *"-"* ]]; then
  count=$(($count+1))
# accounts balance --acc=$1 | grep "| $1"
echo "$val1  $val2    $val3  $val4"
echo " $val1        $val2           $val4 " >> SU_log-$file_ext
fi
}
filecontent=( `ls /project`)

echo " pi-account        Allocation         Balance  " > SU_log-$file_ext
echo " -----------------------------------------------------" >> SU_log-$file_ext
for t in "${filecontent[@]}"
do
 get_balance "pi-$t "
done
#
# EOF
#